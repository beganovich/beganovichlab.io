---
title: What is SSL? With example.
date: 2018-07-25 16:48:25
tags:
---

A lot of time, you will see a red warning in your browser which points something like: Your connection to this site is not secure. 

Yes, I am talking about SSL (or https://).
But, what a heck is SSL? What is the point? How does it work? 

Let's see. 
First, SSL is mostly used on site which is using some kind of data to transfer. For example, that could be payment, user login or anything else which has interaction with the server.

But, how does SSL protect us practically?
Well, it's pretty simple actually. 

For example, on the login form. You have to type your e-mail address, your password, and on themselves are unencrypted or they are in plain text (human-readable). 

Now, you have to send that information to the server, where the server which checks all kind of stuff and gives you the response.  Pretty simple, huh?

But, let's say I am controlling the network. The same one, you are using right now. If you are sending all that data within HTTP, non-secure protocol, I'll know what you just typed, which isn't cool. 

Translated:
You log in to Facebook on the network, which I am controlling. Yes, you would be able to, absolutely normal, but the problem is I'll know your e-mail AND password.

Now, get back to the story. How does SSL protect us? 
Well, over the secured (HTTPs) network when you hit the magical "Log in" button, all information became encrypted, which means, even If I am controlling the network, instead of "your[at]email.com" I'll get something like "5611rcsca3%413&$#$" (these are randomly typed chars, not real encryption). 

To assume, this is why you shouldn't ever sign in public places, such as coffees, airports, restaurants, and similar.

I am pretty sure, you have heard that sentence, multiple time from your operating system.
